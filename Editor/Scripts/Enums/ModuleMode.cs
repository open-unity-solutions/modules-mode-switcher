namespace Valhalla.ModuleModeSwitcher.Editor
{
	public enum ModuleMode
	{
		RegistryPackage = 0,
		Submodule = 1,
		GitPackage = 2,
	}
}

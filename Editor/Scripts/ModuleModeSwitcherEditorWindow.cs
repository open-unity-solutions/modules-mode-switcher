using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using Sirenix.OdinInspector;
using Sirenix.OdinInspector.Editor;
using Sirenix.Utilities;
using Sirenix.Utilities.Editor;
using UnityEditor;
using UnityEditor.PackageManager;
using UnityEngine;
using Valhalla.AssetManagement.Editor;
using Valhalla.ModuleModeSwitcher.Editor.RequestWrappers;
using Debug = UnityEngine.Debug;


namespace Valhalla.ModuleModeSwitcher.Editor
{
	public class ModuleModeSwitcherEditorWindow : OdinMenuEditorWindow
	{
		private const string SELF_PACKAGE_NAME = "com.valhalla.module-mode-switcher";
		private const string SWITCH_SCRIPT_PATH = "Editor/PythonScripts~/switch.py";
		
		private const string DEFAULT_CONFIG_DIRECTORY_PATH = "Assets/Plugins/Valhalla/Modules Mode Switcher";
		private const string DEFAULT_CONFIG_NAME = "modules_config";
		
		
		[SerializeField]
		[InlineEditor(InlineEditorObjectFieldModes.Boxed, Expanded = true)]
		private ModulesConfig _modules;

		
		private ListRequestWrapper _listRequest;


		[MenuItem("Tools/Module Mode Switcher")]
		private static void OpenWindow()
		{
			var window = GetWindow<ModuleModeSwitcherEditorWindow>();

			window.position = GUIHelper.GetEditorWindowRect().AlignCenter(700, 700);
		}
		
		
		[UnityEditor.Callbacks.DidReloadScripts]
		private static void OnScriptsReloaded()
		{
			if (HasOpenInstances<ModuleModeSwitcherEditorWindow>())
				GetWindow<ModuleModeSwitcherEditorWindow>(false).UpdateContent();
		}
		

		public static void Switch(ModuleData module)
		{
			Debug.Log($"Switching {module.SubmoduleName} to {module.Mode}");

			var args = GetArgs(module);

			var psi = new ProcessStartInfo();
			psi.FileName = "python3";
			psi.UseShellExecute = false;
			psi.RedirectStandardOutput = true;
			psi.RedirectStandardError = true;
			psi.Arguments = args;
			
			Debug.Log($"Running command:\npython3 {args}");

			var p = Process.Start(psi);
			var strOutput = p.StandardOutput.ReadToEnd();
			var errOutput = p.StandardError.ReadToEnd();
			p.WaitForExit();
			
			Debug.Log(strOutput);

			if (!string.IsNullOrEmpty(errOutput))
				Debug.LogError(errOutput);
			
			EditorUtility.RequestScriptReload();
		}


		public static string GetArgs(ModuleData module)
		{
			var pythonScriptPath = Path.GetFullPath($"Packages/{SELF_PACKAGE_NAME}/{SWITCH_SCRIPT_PATH}");
			var projectPath = $"{Application.dataPath}";
			
			var orderedArgs = new object[]
			{
				pythonScriptPath,
				projectPath,
				module.PackageName,
				module.SubmoduleName,
				module.SubmodulePath,
				module.GitHttpUrl,
				module.GitSshUrl,
				module.Version,
				module.ModeIndex,
			};
			
			return string.Join(" ", orderedArgs);
		}


		private void Init()
		{
			if (_modules == null)
			{
				_modules = AssetDB.GetAnyOrCreateAtPath<ModulesConfig>(DEFAULT_CONFIG_DIRECTORY_PATH, DEFAULT_CONFIG_NAME);
				_modules.SetupUpdate();
			}

			_listRequest = new ListRequestWrapper(_modules, OnPackageCollectionRead);
			_listRequest.DoRequest();
			
			Undo.undoRedoPerformed += UpdateContent;
		}


		protected override void OnDestroy()
		{
			base.OnDestroy();
			
			Undo.undoRedoPerformed -= UpdateContent;
		}


		private void OnPackageCollectionRead(PackageCollection packages)
		{
			// TODO handle info prom package list
		}
		

		protected override void DrawMenu()
		{
			base.DrawMenu();
			
			GUILayout.BeginHorizontal();
			GUILayout.FlexibleSpace();
			
			if (GUILayout.Button("↻", GUILayout.Width(35)))
				UpdateContent();
			
			if (GUILayout.Button("+", GUILayout.Width(35)))
			{
				Undo.RegisterCompleteObjectUndo(_modules, $"Adding new {nameof(ModuleData)}");
				_modules.AddEntry();
				UpdateContent();
			}

			GUILayout.FlexibleSpace();
			GUILayout.EndHorizontal();
		}


		protected void UpdateContent()
		{
			this.ForceMenuTreeRebuild();

			foreach (var module in _modules.Modules)
				module.UpdateRemoteInfo();
		}


		protected override OdinMenuTree BuildMenuTree()
		{
			Init();

			OdinMenuTree tree = new OdinMenuTree(supportsMultiSelect: false);

			
			var packageNameCounters = new Dictionary<string, int>();
			
			foreach (var module in _modules.Modules)
			{
				string packageName = module.PackageName;
				
				if (packageNameCounters.ContainsKey(packageName))
				{
					var count = packageNameCounters[packageName]++;
					packageName += $" ({count})";
				}
				else
					packageNameCounters.Add(packageName, 1);
				
				var menuLine = tree.AddObjectAtPath(packageName, module).First();

				menuLine.SdfIcon = module.Mode switch
				{
					ModuleMode.RegistryPackage => SdfIconType.CloudDownload,
					ModuleMode.Submodule => SdfIconType.FileEarmarkCodeFill,
					ModuleMode.GitPackage => SdfIconType.Git,
					_ => throw new ArgumentOutOfRangeException(),
				};
				
				
				menuLine.OnRightClick += _ => OnRightMenuClick(module);
			} 

			return tree;
		}


		#region Context Menu
		

		private void OnRightMenuClick(ModuleData moduleData)
		{
			GenericMenu menu = new GenericMenu();

			AddContextMenuLine(menu, "Copy", CopyContextAction,  moduleData);
			
			if (IsPasteContextActionPossible())
				AddContextMenuLine(menu, "Paste", PasteContextAction,  moduleData);
			else
				AddDisabledContextMenuLine(menu, "Paste");
			
			menu.AddSeparator("");
			AddContextMenuLine(menu, "Delete", DeleteLineContextAction,  moduleData);
			
			menu.AddSeparator("");
			AddContextMenuLine(menu, "Remote/Update", UpdateRemoteContextAction,  moduleData);
			
			menu.ShowAsContext();
		}


		private bool IsPasteContextActionPossible()
		{
			try
			{
				JsonUtility.FromJson<ModuleData>(GUIUtility.systemCopyBuffer);
				return true;
			}
			catch (Exception e)
			{
				return false;
			}
		}


		private void AddContextMenuLine(GenericMenu menu, string menuPath, Action<ModuleData> action, ModuleData targetModuleData)
			=> menu.AddItem(new GUIContent(menuPath), false, obj => action.Invoke((ModuleData)obj), targetModuleData);
		
		
		private void AddDisabledContextMenuLine(GenericMenu menu, string menuPath)
			=> menu.AddDisabledItem(new GUIContent(menuPath));
		

		private void DeleteLineContextAction(ModuleData target)
		{
			Debug.Log($"Deleting {target}");
			
			Undo.RecordObject(_modules, $"Deleting {nameof(ModuleData)} {target}");
			_modules.RemoveData(target);
			ForceMenuTreeRebuild();
		}
		
		
		private void UpdateRemoteContextAction(ModuleData target)
		{
			Debug.Log($"Updating {target}");
			target.UpdateRemoteInfo();
		}


		private void CopyContextAction(ModuleData target)
		{
			GUIUtility.systemCopyBuffer = JsonUtility.ToJson(target);
			Debug.Log($"<{target.PackageName}> copied to clipboard");
		}
		
		
		private void PasteContextAction(ModuleData target)
		{
			Undo.RecordObject(_modules, $"Pasting {nameof(ModuleData)} from clipboard");
			try
			{
				EditorJsonUtility.FromJsonOverwrite(GUIUtility.systemCopyBuffer, target);
			}
			catch (Exception e)
			{
				Debug.LogException(e);
				return;
			}
			
			Debug.Log($"<{target.PackageName}> pasted from clipboard");
			_modules.SaveAsset();
			ForceMenuTreeRebuild();
		}
		

		#endregion
	}
}

using System;
using Sirenix.OdinInspector;
using UnityEditor;
using UnityEditor.PackageManager;
using UnityEditor.PackageManager.Requests;
using UnityEngine;


namespace Valhalla.ModuleModeSwitcher.Editor.RequestWrappers
{
	public abstract class RequestWrapper<TRequest, TOutput, TContextData>
		where TRequest : Request
	{
		protected abstract TRequest StartRequest();
		protected abstract void HandleSuccessResponse(Action<TOutput> onSuccessCallback);
		
		
		protected TRequest _request;
		protected readonly TContextData _context;
		private readonly Action<TOutput> _onSuccessCallback;
		
		
		[ShowInInspector, ReadOnly]
		[LabelWidth(120)]
		private bool _wasRequested = false;
		
		[ShowInInspector, ReadOnly]
		[LabelWidth(120)]
		private bool _isRequesting = false;


		public bool WasRequested
			=> _wasRequested;
		
		
		public bool IsRequesting
			=> _isRequesting;


		protected RequestWrapper(TContextData context, Action<TOutput> onSuccessCallback)
		{
			_context = context;
			_onSuccessCallback = onSuccessCallback;
		}
		

		public void DoRequest()
		{
			if (_isRequesting)
				Debug.LogWarning($"Request of {_context} is in progress");

			_wasRequested = true;
			_isRequesting = true;
			_request = StartRequest();
			EditorApplication.update += HandleRequest;
		}


		private void HandleRequest()
		{
			if (!_isRequesting)
				return;

			if (_request.IsCompleted)
			{
				if (_request.Status == StatusCode.Success)
				{
					try
					{
						HandleSuccessResponse(_onSuccessCallback);
					}
					catch
					{
						Debug.LogError($"Can't read package server response");
					}
				}
				else if (_request.Status >= StatusCode.Failure)
					Debug.Log(_request.Error.message);
				
				FinishRequesting();
			}
		}


		private void FinishRequesting()
		{
			EditorApplication.update -= HandleRequest;
			_isRequesting = false;
		}
	}
}

using System;
using UnityEditor;
using UnityEditor.PackageManager;
using UnityEditor.PackageManager.Requests;
using PackageInfo = UnityEditor.PackageManager.PackageInfo;


namespace Valhalla.ModuleModeSwitcher.Editor.RequestWrappers
{
	public class AddRequestWrapper : RequestWrapper<AddRequest, PackageInfo, ModuleData>
	{
		private string _identifier;
		
		public AddRequestWrapper(string package, string version, ModuleData context, Action<PackageInfo> onSuccessCallback) : base(context, onSuccessCallback)
		{
			_identifier = $"{package}@{version}";
		}


		protected override AddRequest StartRequest()
			=> Client.Add(_identifier);


		protected override void HandleSuccessResponse(Action<PackageInfo> onSuccessCallback)
		{
			onSuccessCallback.Invoke(_request.Result);
		}
	}
}

using System;
using UnityEditor.PackageManager;
using UnityEditor.PackageManager.Requests;


namespace Valhalla.ModuleModeSwitcher.Editor.RequestWrappers
{
	public class ListRequestWrapper : RequestWrapper<ListRequest, PackageCollection, ModulesConfig>
	{
		public ListRequestWrapper(ModulesConfig context, Action<PackageCollection> onSuccessCallback) : base(context, onSuccessCallback)
		{
		}


		protected override ListRequest StartRequest()
			=> Client.List(false, false);


		protected override void HandleSuccessResponse(Action<PackageCollection> onSuccessCallback)
		{
			onSuccessCallback.Invoke(_request.Result);
		}
	}
}

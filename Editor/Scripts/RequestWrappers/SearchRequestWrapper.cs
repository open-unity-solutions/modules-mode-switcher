using System;
using UnityEditor.PackageManager;
using UnityEditor.PackageManager.Requests;
using UnityEngine;
using PackageInfo = UnityEditor.PackageManager.PackageInfo;


namespace Valhalla.ModuleModeSwitcher.Editor.RequestWrappers
{
	public class SearchRequestWrapper : RequestWrapper<SearchRequest, PackageInfo, ModuleData>
	{
		public SearchRequestWrapper(ModuleData context, Action<PackageInfo> onSuccessCallback) : base(context, onSuccessCallback)
		{
			
		}
		
		
		protected override SearchRequest StartRequest()
			=> Client.Search(_context.PackageName);


		protected override void HandleSuccessResponse(Action<PackageInfo> onSuccessCallback)
		{
			var response = _request.Result;

			if (response == null || response.Length == 0)
				Debug.LogWarning($"No {_context.PackageName} package found!");
			else if (response.Length > 1)
				Debug.LogWarning($"More than one {_context.PackageName} package found!");
			else
				onSuccessCallback.Invoke(response[0]);
		}
		
	}
}

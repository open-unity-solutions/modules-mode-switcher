using Sirenix.OdinInspector;
using UnityEditor;
using UnityEditor.PackageManager;
using UnityEngine;
using Valhalla.ModuleModeSwitcher.Editor.RequestWrappers;
using PackageInfo = UnityEditor.PackageManager.PackageInfo;


namespace Valhalla.ModuleModeSwitcher.Editor
{
	[HideReferenceObjectPicker]
	public class VersionData
	{
		private readonly string _version;

		private readonly ModuleData _context;

		private readonly PackageInfo _package;
		
		private AddRequestWrapper _addRequest;

		private bool _isCurrent;


		[ShowInInspector]
		[HideLabel, HorizontalGroup]
		private string VersionLabel
			=> _isCurrent
				? $"[{_version}]"
				: _version;
		
		
		public VersionData(string version, ModuleData context, PackageInfo package, bool isCurrent = false)
		{
			_version = version;
			_package = package;
			_context = context;

			_isCurrent = isCurrent;
			
			_addRequest = new AddRequestWrapper(package.name, version, context, OnAddComplete);
		}


		[Button("Set")]
		[EnableIf(nameof(IsButtonClickable)), HorizontalGroup]
		private void SetVersion()
		{
			Debug.Log($"Setting version of {_package.name} to {_version}");
			
			_addRequest.DoRequest();
		}
		
		
		private void OnAddComplete(PackageInfo packageInfo)
		{
			Debug.Log($"Added {packageInfo.packageId}@{packageInfo.version}");
			_context.SetVersion(packageInfo.version);
		}


		private bool IsButtonClickable()
		{
			if (_addRequest.IsRequesting)
				return false;

			if (_context.Mode != ModuleMode.RegistryPackage)
				return false;

			if (_isCurrent)
				return false;

			return true;
		}
	}
}

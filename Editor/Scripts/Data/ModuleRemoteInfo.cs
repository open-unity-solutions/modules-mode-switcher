using System.Collections.Generic;
using System.Linq;
using Sirenix.OdinInspector;
using UnityEditor;
using Valhalla.ModuleModeSwitcher.Editor.RequestWrappers;
using PackageInfo = UnityEditor.PackageManager.PackageInfo;


namespace Valhalla.ModuleModeSwitcher.Editor
{
	public class ModuleRemoteInfo
	{
		[ShowInInspector]
		[HideReferenceObjectPicker]
		private SearchRequestWrapper _searchRequest;

		private ModuleData _context;

		

		[ShowInInspector]
		[ListDrawerSettings(IsReadOnly = true)]
		private List<VersionData> _versions = new();


			
			
		public void LoadData(ModuleData context)
		{
			_context = context;

			if (!context.IsRegistryEnabled)
				return;

			if (_searchRequest == null)
				_searchRequest = new SearchRequestWrapper(context, HandleSuccessResponse);
			_searchRequest.DoRequest();
		}
		
		
		private void HandleSuccessResponse(PackageInfo packageInfo)
		{
			_versions = packageInfo.versions.compatible
				.Select(version => new VersionData(version, _context, packageInfo, version == packageInfo.version))
				.ToList();
			
			_versions.Reverse();
		}
		
		
	}
}

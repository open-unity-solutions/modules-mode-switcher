using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEditor;
using UnityEngine;


namespace Valhalla.ModuleModeSwitcher.Editor
{
	[CreateAssetMenu(fileName = "modules_config", menuName = "Valhalla/Modules Config")]
	public class ModulesConfig : ScriptableObject
	{
		[SerializeField, Required]
		private List<ModuleData> _modules = new();
		
		
		public IReadOnlyList<ModuleData> Modules
			=> _modules;


		public void SetupUpdate()
		{
			if (_modules == null)
			{
				_modules = new List<ModuleData>();
				SaveAsset();
			}
			
			foreach (var moduleData in _modules)
				moduleData.SetContext(this);
		}


		public void AddEntry()
		{
			var newData = new ModuleData();
			newData.SetContext(this);
			_modules.Add(newData);
			
			SaveAsset();
		}


		public void RemoveData(ModuleData data)
		{
			_modules.Remove(data);
			SaveAsset();
		}


		public void SaveAsset()
		{
			EditorUtility.SetDirty(this);
			AssetDatabase.SaveAssetIfDirty(this);
		}
	}
}

using System;
using Sirenix.OdinInspector;
using UnityEditor;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEditor.PackageManager.Requests;
using UnityEditor.PackageManager;


namespace Valhalla.ModuleModeSwitcher.Editor
{
	[Serializable]
	public class ModuleData
	{
		private const string HORIZONTAL = "Horizontal";
		private const string VERTICAL_LEFT = HORIZONTAL + "/Left";
		private const string VERTICAL_RIGHT = HORIZONTAL + "/Right";
		
		private const string GROUP_CONFIG = VERTICAL_LEFT + "/Config";
		private const string GROUP_COMMON = GROUP_CONFIG + "/Common";
		private const string GROUP_SUBMODULE = GROUP_CONFIG + "/Submodule";
		private const string GROUP_REPOSITORY = GROUP_CONFIG + "/Repository";
		
		private const string GROUP_SWITCHER = VERTICAL_LEFT + "/SWITCH";
		
		private const string GROUP_REMOTE = VERTICAL_RIGHT + "/Remote Info";
		
		
		[Title("$" + nameof(__EditorTitleName))]
		[ShowInInspector, PropertyOrder(-100)]
		[HideLabel, DisplayAsString]
		private string __EditorVersionLabel
			=> $"v{_version.x}.{_version.y}.{_version.z}";
		
		
		[HorizontalGroup(HORIZONTAL)]
		[VerticalGroup(VERTICAL_LEFT)]
		
		[FoldoutGroup(GROUP_CONFIG)]
		
		[SerializeField, Required]
		[BoxGroup(GROUP_COMMON)]
		private string _packageName;
		
		
		[SerializeField]
		[LabelWidth(50)]
		[VerticalGroup(VERTICAL_LEFT)]
		private Vector3Int _version;
		
		
		[SerializeField]
		[BoxGroup(GROUP_COMMON)]
		private bool _isRegistryEnabled;


		
		[SerializeField, Required]
		[BoxGroup(GROUP_SUBMODULE)]
		public string SubmoduleName;


		[SerializeField, Required]
		[BoxGroup(GROUP_SUBMODULE)]
		public string SubmodulePath;


		[SerializeField, Required]
		[BoxGroup(GROUP_REPOSITORY)]
		public string GitHost;
		
		
		[SerializeField, Required]
		[BoxGroup(GROUP_REPOSITORY)]
		public string GitRepository;

		
		[SerializeField, ReadOnly]
		[VerticalGroup(VERTICAL_LEFT)]
		private ModuleMode _mode = ModuleMode.RegistryPackage;
		
		
		[ShowInInspector]
		[HideReferenceObjectPicker]
		[ShowIf(nameof(IsRegistryEnabled))]
		[HorizontalGroup(HORIZONTAL, width: 170)]
		[VerticalGroup(VERTICAL_RIGHT)]
		[BoxGroup(GROUP_REMOTE), HideLabel]
		private ModuleRemoteInfo _remoteInfo = new();
		
		private ModulesConfig _contextOwner;



		internal int ModeIndex
			=> (int) _mode;


		public ModuleMode Mode
			=> _mode;


		public string Version
			=> $"v{PureVersion}";
		
		
		public string PureVersion
			=> $"{_version.x}.{_version.y}.{_version.z}";
		
		
		public string PackageName
			=> _packageName;
		
		
		public bool IsRegistryEnabled
			=> _isRegistryEnabled;


		private string __EditorTitleName
			=> !string.IsNullOrEmpty(PackageName)
				? PackageName
				: "Unnamed module";


		public string GitHttpUrl
			=> $"https://{GitHost}/{GitRepository}.git";
		
		
		public string GitSshUrl
			=> $"git@{GitHost}:{GitRepository}.git";


		public ModuleData()
		{
			_packageName = "Unnamed module";
			_version = new Vector3Int(0, 0, 0);
		}
		

		[Button("Registry"), ButtonGroup(GROUP_SWITCHER)]
		[GUIColor(nameof(RegistryButtonColor)), ShowIf(nameof(_isRegistryEnabled))]
		private void SwitchToRegistry()
			=> Switch(ModuleMode.RegistryPackage);
		
		
		[Button("Submodule"), ButtonGroup(GROUP_SWITCHER)]
		[GUIColor(nameof(SubmoduleButtonColor))]
		private void SwitchToSubmodule()
			=> Switch(ModuleMode.Submodule);
		
		
		[Button("Git"), ButtonGroup(GROUP_SWITCHER)]
		[GUIColor(nameof(GitButtonColor))]
		private void SwitchToGit()
			=> Switch(ModuleMode.GitPackage);
		

		private void Switch(ModuleMode mode)
		{
			if (mode == ModuleMode.RegistryPackage && !_isRegistryEnabled)
				throw new Exception("Registry is disabled for this module");
			
			
			_mode = mode;
			
			if (_contextOwner != null)
				_contextOwner.SaveAsset();
			
			ModuleModeSwitcherEditorWindow.Switch(this);
		}

		
		private Color RegistryButtonColor
			=> GetButtonColor(ModuleMode.RegistryPackage);
		
		
		private Color SubmoduleButtonColor
			=> GetButtonColor(ModuleMode.Submodule);
		
		
		private Color GitButtonColor
			=> GetButtonColor(ModuleMode.GitPackage);


		private Color GetButtonColor(ModuleMode mode)
			=> _mode == mode
				? Color.white
				: new Color(0.85f, 0.85f, 0.85f);


		public void SetContext(ModulesConfig context)
		{
			_contextOwner = context;
			UpdateRemoteInfo();
		}


		public void UpdateRemoteInfo()
		{
			_remoteInfo.LoadData(this);
		}
		
		
		internal void SetVersion(string version)
		{
			var split = version.Split('.');
			if (split.Length != 3)
				throw new Exception("Invalid version format");
			
			_version = new Vector3Int(
				int.Parse(split[0]),
				int.Parse(split[1]),
				int.Parse(split[2])
			);
		}
	}
}

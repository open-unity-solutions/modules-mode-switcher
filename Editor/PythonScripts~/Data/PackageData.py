from pathlib import Path


class PackageData:
    project_path: Path  # FIXME not related
    package_name: str
    submodule_name: str
    submodule_path: Path
    submodule_local_path: Path
    git_http_url: str  # FIXME not related
    git_ssh_url: str  # FIXME not related
    version: str
    manifest_path: Path  # FIXME not related
    gitmodules_path: Path  # FIXME not related
    gitconfig_path: Path  # FIXME not related

    def __init__(self, project_path: Path, package_name: str, submodule_name: str, submodule_path: Path, git_http_url: str, git_ssh_url: str, version: str):
        self.project_path = project_path
        self.package_name = package_name
        self.submodule_name = submodule_name
        self.submodule_local_path = submodule_path
        self.submodule_path = self.project_path / submodule_path
        self.git_http_url = git_http_url
        self.git_ssh_url = git_ssh_url
        self.version = version
        self.manifest_path = self.project_path / "Packages" / "manifest.json"
        self.gitmodules_path = self.project_path / '.gitmodules'
        self.gitconfig_path = self.project_path / '.git' / 'config'

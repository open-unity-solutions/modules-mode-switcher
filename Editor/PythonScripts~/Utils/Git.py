import re
import subprocess
from pathlib import Path
from typing import Tuple

from cmd import run_command


class Git:
    project_path: Path
    gitmodules_path: Path
    gitconfig_path: Path

    def __init__(self, project_path: Path, gitmodules_path: Path, gitconfig_path: Path):
        self.project_path = project_path
        self.gitmodules_path = gitmodules_path
        self.gitconfig_path = gitconfig_path

    def add_submodule(self, name: str, local_path: Path, ssh_url: str, checkout_target: str) -> None:
        submodule_full_path = self.project_path / local_path

        run_command(['git', 'submodule', 'add', '--name', name, '--force', ssh_url, local_path], self.project_path)
        run_command(['git', 'remote', 'set-url', 'origin', ssh_url], submodule_full_path)
        run_command(['git', 'fetch'], submodule_full_path)
        run_command(['git', 'checkout', checkout_target], submodule_full_path)
        run_command(['git', 'reset'], self.project_path)

    def remove_submodule(self, name: str) -> None:
        gitmodules_pattern = re.compile(rf'\[submodule\s\"{name}\"]\n\tpath = (.+)\n\turl = (.+)\n')
        git_config_pattern = re.compile(rf'\[submodule\s\"{name}\"]\n\turl = (.+)\n(\tactive = .+\n)?')

        submodule_path, git_url = Git._replace_file_content_with_pattern_and_get_groups(self.gitmodules_path, gitmodules_pattern)

        run_command(['git', 'add', '.gitmodules'], self.project_path)

        Git._replace_file_content_with_pattern_and_get_groups(self.gitconfig_path, git_config_pattern)

        run_command(['rm', '-rf', f'.git/modules/{submodule_path}'], self.project_path)

        run_command(['rm', '-rf', submodule_path], self.project_path)

        if len(self.gitmodules_path.read_text()) == 0:
            run_command(['rm', '-rf', self.gitmodules_path], self.project_path)

        run_command(['git', 'reset'], self.project_path)

    def get_submodules_names(self) -> str:
        process = subprocess.run(['git', 'config', '--file', '.gitmodules', '--name-only', '--get-regexp', 'path'],
                                 stdout=subprocess.PIPE,
                                 stderr=subprocess.PIPE,
                                 universal_newlines=True,
                                 cwd=self.project_path)

        return process.stdout

    def is_in_submodule_mode(self, submodule_name: str) -> bool:
        if self.gitmodules_path.exists():
            regex = re.compile(r"^submodule\.(.+)\.path$")

            output = self.get_submodules_names()
            modules = output.split('\n')

            for (index, value) in enumerate(modules):
                search = re.search(regex, value)

                if search is None:
                    continue

                module = search.group(1)

                if module == submodule_name:
                    return True
        return False

    @staticmethod
    def _replace_file_content_with_pattern_and_get_groups(file_path: Path, pattern: re.Pattern) -> Tuple[str, ...]:
        with open(file_path, 'r') as file:
            file_data = file.read()

        groups = re.search(pattern, file_data).groups()

        file_data = re.sub(pattern, "", file_data)

        with open(file_path, 'w') as file:
            file.write(file_data)

        return groups

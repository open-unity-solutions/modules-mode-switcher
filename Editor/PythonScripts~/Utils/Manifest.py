import json
import re
from pathlib import Path
from typing import Dict

from Mode import Mode

DEPENDENCIES_KEY = "dependencies"


class Manifest:
    content: Dict
    path: Path

    def __init__(self, path: Path):
        self.path = path
        self.content = json.load(self.path.open())

    def get_dependency_mode(self, package_name: str) -> Mode:
        semver_pattern = re.compile(r'\d+\.\d+\.\d+')
        git_link_pattern = re.compile(r'#v\d+\.\d+\.\d+')

        for (index, package_id) in enumerate(self.content[DEPENDENCIES_KEY]):
            link: str = self.content[DEPENDENCIES_KEY][package_id]
            if package_id == package_name:
                print(f'\tFound dependency <{package_id}: {link}>')
                if semver_pattern.match(link):
                    print("  Current state is: registry package")
                    return Mode.registry_package
                if git_link_pattern.search(link):
                    print("  Current state is: git package")
                    return Mode.git_package

        raise Exception("No package found in the manifest")

    def set_dependency_to_git(self, package_name: str, http_url: str, tag: str) -> None:
        self._set_dependency(package_name, f'{http_url}#{tag}')

    def set_dependency_to_registry(self, package_name: str, version_number: str) -> None:
        self._set_dependency(package_name, version_number)

    def set_dependency_to_file(self, package_name: str, path: Path) -> None:
        self._set_dependency(package_name, f'file:{path}')

    def _set_dependency(self, package_name: str, value: str) -> None:
        for (index, package_id) in enumerate(self.content[DEPENDENCIES_KEY]):
            if package_id == package_name:
                self.content[DEPENDENCIES_KEY][package_id] = value
                break

        self._save()

    def _save(self) -> None:
        json.dump(self.content, self.path.open('w'), ensure_ascii=False, indent=2)

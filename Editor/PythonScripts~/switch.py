import sys
from pathlib import Path
from typing import Tuple

from Mode import Mode
from Data.PackageData import PackageData
from Utils.Git import Git
from Utils.Manifest import Manifest


def main() -> None:
    package, requested_mode = read_args()
    git = Git(package.project_path, package.gitmodules_path, package.gitconfig_path)
    manifest = Manifest(package.manifest_path)
    print(f'Module switch initiated for {package.submodule_name}')

    current_mode = check_current_mode(package, git, manifest)

    if requested_mode == current_mode:
        print("Already in needed mode")
    elif requested_mode == Mode.submodule:
        switch_to_submodule(package, git, manifest)
    else:
        switch_to_package(package, git, manifest, current_mode, requested_mode)


def read_args() -> Tuple[PackageData, Mode]:
    project_path = Path(sys.argv[1]).parent
    package_name = sys.argv[2]
    submodule_name = sys.argv[3]
    submodule_path = Path(sys.argv[4].strip('/'))
    git_http_url = sys.argv[5]
    git_ssh_url = sys.argv[6]
    version = sys.argv[7]
    requested_mode = Mode(int(sys.argv[8]))

    package = PackageData(project_path, package_name, submodule_name, submodule_path, git_http_url, git_ssh_url, version)

    return package, requested_mode


def check_current_mode(package: PackageData, git: Git, manifest: Manifest) -> Mode:
    print("Checking current mode...")

    if git.is_in_submodule_mode(package.submodule_name):
        print("  Current mode is: submodule")
        return Mode.submodule
    else:
        print("  Current mode is not submodule")
        return manifest.get_dependency_mode(package.package_name)


def switch_to_package(package: PackageData, git: Git, manifest: Manifest, current_mode: Mode, requested_mode: Mode) -> None:
    print(f'Switching to {requested_mode.name}...')

    if current_mode == Mode.submodule:
        git.remove_submodule(package.submodule_name)

    if requested_mode == Mode.git_package:
        manifest.set_dependency_to_git(package.package_name, package.git_http_url, package.version)
    else:
        manifest.set_dependency_to_registry(package.package_name, package.version[1:])

    print(f'Switched to {requested_mode.name}')


def switch_to_submodule(package: PackageData, git: Git, manifest: Manifest) -> None:
    print("Switching to submodule...")
    git.add_submodule(package.submodule_name, package.submodule_local_path, package.git_ssh_url, f'tags/{package.version}')
    manifest.set_dependency_to_file(package.package_name, package.submodule_path)
    print("Switched to submodule")


if __name__ == '__main__':
    main()

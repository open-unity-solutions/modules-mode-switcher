from enum import Enum


class Mode(Enum):
    registry_package = 0
    submodule = 1
    git_package = 2

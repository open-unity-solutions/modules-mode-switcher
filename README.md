## About

Switcher between:
- scoped registry package
- git-linked package
- local fit submodule

Can be useful, if you want to develop your package in a real game environment.

**Requires Odin Inspector**

**Requires Python 3**


## Usage

1. Open `Tools -> Module Mode Switcher` window
2. On first run, empty config in `"Assets/Plugins/Valhalla"` will be generated
3. Write your package name, version and git submodule parameters for each needed package
4. Click on tumbler to switch package mode

**Registry** mode works with package version, may need to setup `"scopedRegistries"` section in `manifest.json`.

**Submodule** mode works with git submodule on a specified path.

**Git** mode works with git dependency, that tagged with `v1.0.0` (with specified in config version)

## TODO
- [ ] Auto-add missing packages to manifest

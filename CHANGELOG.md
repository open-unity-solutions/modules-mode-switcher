## [1.7.0] – 2023-07-18

### Added
- Context menu for packages in list
- Copy-pasting of packages
- Undo (ctrl+z) support


## [1.6.4] – 2023-07-11

### Added
- Current version (by manifest) is now highlighted with `[ ]`


## [1.6.3] – 2023-07-11

### Removed
- `AssetDatabase` logic wrapped in package `com.valhalla.asset-management`


## [1.6.2] – 2023-07-11

### Added
- It's possible now to set manifest version of package from MMM


## [1.6.1] – 2023-07-03

### Added
- `RequestWrapper` for handling different UPM requests

### Changed
- Editor UI changed to display remote data


## [1.6.0] – 2023-07-03

### Added
- Remote info about module, that loads versions from registry

### Changed
- CI/CD updated for modularity


## [1.5.1]

### Added
- Icons in module editor window, different for each mode

### Changed
- Licence updated to 2023


## [1.5.0]

### Added
- Generation of empty config, if needed
- Warning, if several configs spotted

### Changed
- New editor design based on `OdinMenuEditorWindow`
- Readme updated

### Fixed
- Naming of config SO fixed


## [1.4.0]

### Added
- Separated config of package name and submodule name

### Changed
- Git config now set for host and repository, not for https and ssh
- Properties reorganized
- Argument generation refactored
- Python: git and manifest logic separated


## [1.3.1]

### Fixed
- Typo in package name
- Hardcoded path to self package


## [1.3.0]

### Added
- Titles to list of modules, for better readability

### Changed
- Rebranded from "Open Unity Solutions" to "Valhalla" (breaking change, affects namespaces and package domain)

### Fixed
- Typo in namespace fixed


## [1.2.2]

### Fixed
- Fixed remote ssh set: now it sets origin to submodule, not parent repository

### Changed
- Changelog version formatting changed


## [1.2.1]

### Changed
- Minor package info updates

### Fixed
- Fixed typo in context menu
- Changelog rewritten to match standard


## [1.2.0]

### Added
- Added option to disable the registry mode (for non-registry packages)

### Changed
- Now for submodules used SSH git url, to ensure smooth authorization on gitlab

### Fixed
- Modes are buttons now, so you can press mode twice


## [1.1.1]

### Fixed
- Fixed manifest change on `registry-package` -> `submodule` switch


## [1.1.0]

### Added
- Scoped Registry mode support

### Fixed
- Incorrect creation path of SO fixed


## [1.0.1]

### Added
- Removed CI/CD submodule from sources


## [1.0.0]

### Added
- Package created
